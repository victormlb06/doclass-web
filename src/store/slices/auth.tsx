import { createSlice } from "@reduxjs/toolkit"

const auth = createSlice({
  name: "auth",
  initialState: {
    email: "test@fernando.com",
  },
  reducers: {
    setAuthentication(state, action) {
      return {
        ...state,
        ...action.payload,
      }
    },
  },
})

export const { setAuthentication } = auth.actions
export default auth.reducer