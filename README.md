# Doclass

A tool for the interactive annotation and classification of juridic documents

## Doclass projects

You can see doclass-web (this repo), [doclass-mobile](https://gitlab.com/ivato/doclass/doclass-backend) and [doclass-backend](https://gitlab.com/ivato/doclass/doclass-mobile)

### Prerequisites

- NodeJS
- Yarn (Recommended)

### Starting this project

```bash
Clone this project
$ git clone https://gitlab.com/ivato/doclass/doclass-web

Access the project folder
$ cd doclass-web

Install the dependencies
$ yarn
or
$ npm install

lauch the frontend web application
$ yarn start
or
$ npm start

```
